package com.craftinginterpreters.lox;

import java.util.List;

class AstPrinter implements Expr.Visitor<String>, Stmt.Visitor<String> {
	String print(List<Stmt> statements) {
		return printBlock("program", statements);
	}

	@Override
	public String visitAssignExpr(Expr.Assign expr) {
		return parenthesize("assign", expr.name.lexeme, expr.value);
	}

	@Override
	public String visitBinaryExpr(Expr.Binary expr) {
		return parenthesize(expr.operator.lexeme, expr.left, expr.right);
	}

	@Override
	public String visitCallExpr(Expr.Call expr) {
		return parenthesize(expr.callee, expr.arguments);
	}

	@Override
	public String visitGetExpr(Expr.Get expr) {
		return parenthesize("get", expr.name.lexeme, expr.object);
	}

	@Override
	public String visitGroupingExpr(Expr.Grouping expr) {
		return parenthesize("group", expr.expression);
	}

	@Override
	public String visitLiteralExpr(Expr.Literal expr) {
		if (expr.value == null) return "nil";
		return expr.value.toString();
	}

	@Override
	public String visitLogicalExpr(Expr.Logical expr) {
		return parenthesize(expr.operator.lexeme, expr.left, expr.right);
	}

	@Override
	public String visitSetExpr(Expr.Set expr) {
		return parenthesize("set", expr.name.lexeme, expr.object, expr.value);
	}

	@Override
	public String visitSuperExpr(Expr.Super expr) {
		return "(super " + expr.method.lexeme + ")";
	}

	@Override
	public String visitThisExpr(Expr.This expr) {
		return "(this)";
	}

	@Override
	public String visitUnaryExpr(Expr.Unary expr) {
		return parenthesize(expr.operator.lexeme, expr.right);
	}

	@Override
	public String visitVariableExpr(Expr.Variable expr) {
		return parenthesize(expr.name.lexeme);
	}

	private String parenthesize(Expr callee, List<Expr> arguments) {
		return parenthesize(callee.accept(this), arguments.toArray(new Expr[0]));
	}

	private String parenthesize(String name, Expr... exprs) {
		StringBuilder builder = new StringBuilder();

		builder.append("(").append(name);
		for (Expr expr : exprs) {
			builder.append(" ");
			builder.append(expr.accept(this));
		}
		builder.append(")");

		return builder.toString();
	}

	private String parenthesize(String name, String varName, Expr... exprs) {
		StringBuilder builder = new StringBuilder();

		builder.append("(").append(name)
			.append(" ").append(varName);
		for (Expr expr : exprs) {
			builder.append(" ");
			builder.append(expr.accept(this));
		}
		builder.append(")");

		return builder.toString();
	}

	String printBlock(String name, List<Stmt> statements) {
		StringBuilder builder = new StringBuilder();

		builder.append("(").append(name);
		for (Stmt statement : statements) {
			builder.append("\n");
			builder.append(statement.accept(this));
		}
		builder.append(")");

		return builder.toString();
	}

	@Override
	public String visitBlockStmt(Stmt.Block stmt) {
		return printBlock("block", stmt.statements);
	}

	@Override
	public String visitClassStmt(Stmt.Class stmt) {
		StringBuilder builder = new StringBuilder();
		builder.append("(class ")
			.append(stmt.name);
		for (Stmt.Function method : stmt.methods) {
			builder.append("\n(method ")
				.append(printBlock(method.name.lexeme, method.body))
				.append(")");
		}
		builder.append(")");

		return builder.toString();
	}

	@Override
	public String visitExpressionStmt(Stmt.Expression stmt) {
		return parenthesize("stmt", stmt.expression);
	}

	@Override
	public String visitFunctionStmt(Stmt.Function stmt) {
		return parenthesize("decl", printBlock(stmt.name.lexeme, stmt.body));
	}

	@Override
	public String visitIfStmt(Stmt.If stmt) {
		return "(if " + stmt.condition.accept(this) + ", "
			+ stmt.thenBranch.accept(this) + ", "
			+ (stmt.elseBranch != null ? stmt.elseBranch.accept(this) : "(block)")
			+ ")";
	}

	@Override
	public String visitPrintStmt(Stmt.Print stmt) {
		return parenthesize("print", stmt.expression);
	}

	@Override
	public String visitReturnStmt(Stmt.Return stmt) {
		return parenthesize("return", stmt.value);
	}

	@Override
	public String visitVarStmt(Stmt.Var stmt) {
		return parenthesize("decl", stmt.name.lexeme, stmt.initializer);
	}

	@Override
	public String visitWhileStmt(Stmt.While stmt) {
		return "(while " + stmt.condition.accept(this) + ", "
			+ stmt.body.accept(this)
			+ ")";
	}
}
